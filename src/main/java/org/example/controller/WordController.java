package org.example.controller;

import org.example.dto.WordDto;
import org.example.entity.DictionaryType;
import org.example.service.api.DictionaryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/words")
public class WordController {

    private final DictionaryService dictionaryService;

    public WordController(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    @GetMapping
    public ResponseEntity<List<WordDto>> getWords(@RequestParam DictionaryType type) {
        return ResponseEntity.ok(dictionaryService.getAllWords(type));
    }

    @GetMapping("/translation")
    public ResponseEntity<List<WordDto>> getTranslation(WordDto dto) {
        return ResponseEntity.ok(dictionaryService.getTranslations(dto));
    }

    @PostMapping
    public ResponseEntity<WordDto> createWord(@RequestBody WordDto word) {
        return ResponseEntity.status(HttpStatus.CREATED).body(dictionaryService.createWord(word));
    }

    @PutMapping
    public ResponseEntity<WordDto> updateTranslation(@RequestBody WordDto word) {
        return ResponseEntity.ok((dictionaryService.updateWord(word)));
    }

    @DeleteMapping
    public ResponseEntity<Object> removeWord(@RequestBody WordDto dto) {
        dictionaryService.deleteWord(dto);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
