package org.example.service.impl;

import org.example.dto.WordDto;
import org.example.entity.DictionaryType;
import org.example.entity.Word;
import org.example.exception.NotFoundException;
import org.example.mapper.WordMapper;
import org.example.repository.api.DictionaryRepository;
import org.example.service.api.DictionaryService;
import org.example.validator.api.WordsValidator;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DictionaryServiceImpl implements DictionaryService {

    private final WordsValidator validator;
    private final DictionaryRepository dictionaryRepository;

    public DictionaryServiceImpl(WordsValidator validator, DictionaryRepository dictionaryRepository) {
        this.validator = validator;
        this.dictionaryRepository = dictionaryRepository;
    }

    @Override
    public List<WordDto> getAllWords(DictionaryType dictionaryType) {
        return WordMapper.toDtos(dictionaryRepository.findAllByDictionaryType(dictionaryType));
    }

    @Override
    public List<WordDto> getTranslations(WordDto dto) {
        return WordMapper.toDtos(dictionaryRepository.findAllByOriginal(dto.getOriginal()));
    }

    @Override
    public WordDto createWord(WordDto dto) {
        validator.validate(dto);
        dictionaryRepository.save(WordMapper.toEntity(dto));
        return dto;
    }

    @Override
    public WordDto updateWord(WordDto dto) {
        validator.validate(dto);
        Word wordToUpdate = dictionaryRepository.findAllByOriginal(dto.getOriginal())
                .stream().findFirst().orElseThrow(() -> new NotFoundException(dto.getOriginal()));
        wordToUpdate.setTranslation(dto.getTranslation());
        dictionaryRepository.save(wordToUpdate);
        return dto;
    }

    @Override
    public void deleteWord(WordDto dto) {
        validator.validate(dto);
        dictionaryRepository.delete(WordMapper.toEntity(dto));
    }
}
