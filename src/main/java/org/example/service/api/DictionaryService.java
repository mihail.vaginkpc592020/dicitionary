package org.example.service.api;

import org.example.dto.WordDto;
import org.example.entity.DictionaryType;

import java.util.List;

public interface DictionaryService {

    List<WordDto> getAllWords(DictionaryType dictionaryType);

    List<WordDto> getTranslations(WordDto dto);

    WordDto createWord(WordDto word);

    WordDto updateWord(WordDto word);

    void deleteWord(WordDto word);

}
