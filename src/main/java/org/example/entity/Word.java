package org.example.entity;

import jakarta.persistence.*;
import org.hibernate.annotations.SQLDelete;

import java.util.UUID;


@Entity
@SQLDelete(sql = "update word set deleted=true where uuid=?")
public class Word {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID uuid;

    @Column(nullable = false)
    private String original;
    @Column(nullable = false)
    private String translation;
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private DictionaryType dictionaryType;
    @Column(nullable = false)
    private Boolean deleted = false;

    public void setOriginal(String value) {
        original = value;
    }

    public String getOriginal() {
        return original;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    @Override
    public String toString() {
        return String.format("%s:%s", original, translation);
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public DictionaryType getDictionaryType() {
        return dictionaryType;
    }

    public void setDictionaryType(DictionaryType dictionaryType) {
        this.dictionaryType = dictionaryType;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public UUID getUuid() {
        return uuid;
    }
}
