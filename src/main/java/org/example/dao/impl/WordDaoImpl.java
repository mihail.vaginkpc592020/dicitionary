package org.example.dao.impl;

import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.example.dao.api.DictionaryDao;
import org.example.entity.DictionaryType;
import org.example.entity.Word;
import org.example.entity.Word_;
import org.example.exception.NotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("word")
public class WordDaoImpl implements DictionaryDao<Word> {

    private final EntityManager entityManager;
    private final CriteriaQuery<Word> criteriaQuery;
    private final CriteriaBuilder criteriaBuilder;
    private final Root<Word> wordRoot;
    private final Predicate notDeleted;

    public WordDaoImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery(Word.class);
        wordRoot = criteriaQuery.from(Word.class);
        notDeleted = criteriaBuilder.equal(wordRoot.get(Word_.deleted), false);
    }

    @Override
    public List<Word> getAll(DictionaryType type) {
        criteriaQuery.select(wordRoot).where(
                criteriaBuilder.and(criteriaBuilder.equal(wordRoot.get(Word_.dictionaryType), type) ,notDeleted));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public List<Word> getTranslations(String original) {
        criteriaQuery.select(wordRoot)
                .where(criteriaBuilder.and(notDeleted,
                        criteriaBuilder.equal(wordRoot.get(Word_.original), original)));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public void createWord(Word word) {
        entityManager.persist(word);
    }

    @Override
    public void updateWord(Word word) {
        word = findByOriginal(word);
        entityManager.merge(word);
    }

    @Override
    public void deleteWord(Word word) {
        word = findByOriginal(word);
        word.setDeleted(true);
        entityManager.merge(word);
    }

    private Word findByOriginal(Word word) {
        Predicate name = criteriaBuilder.equal(wordRoot.get(Word_.original), word.getOriginal());
        Predicate dictionaryType = criteriaBuilder.equal(wordRoot.get(Word_.dictionaryType), word.getDictionaryType());

        criteriaQuery.select(wordRoot).where(criteriaBuilder.and(name, dictionaryType, notDeleted));
        return entityManager.createQuery(criteriaQuery).getResultList()
                .stream()
                .findFirst().orElseThrow(() -> new NotFoundException(word.getOriginal()));
    }
}
