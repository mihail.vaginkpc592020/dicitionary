package org.example.dao.api;

import org.example.entity.DictionaryType;
import org.example.entity.Word;

import java.util.List;

public interface DictionaryDao<T> {

    List<T> getTranslations(String translation);
    List<T> getAll(DictionaryType type);
    void createWord(Word word);
    void updateWord(Word word);
    void deleteWord(Word word);
}
