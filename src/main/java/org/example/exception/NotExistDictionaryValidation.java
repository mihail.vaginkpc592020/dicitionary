package org.example.exception;

public class NotExistDictionaryValidation extends RuntimeException {

    public NotExistDictionaryValidation(String validationName) {
        super(String.format("Нет способа валидации для словаря типа %s", validationName));
    }
}
