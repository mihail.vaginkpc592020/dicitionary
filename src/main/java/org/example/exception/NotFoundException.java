package org.example.exception;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String original) {
        super(String.format("%s не найдено", original));
    }
}
