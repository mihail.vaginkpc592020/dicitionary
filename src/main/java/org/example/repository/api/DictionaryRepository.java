package org.example.repository.api;

import org.example.entity.DictionaryType;
import org.example.entity.Word;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface DictionaryRepository extends JpaRepository<Word, UUID> {

    List<Word> findAllByDictionaryType(DictionaryType dictionaryType);
    List<Word> findAllByOriginal(String original);
}
