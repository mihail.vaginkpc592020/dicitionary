package org.example.mapper;

import org.example.builder.WordBuilder;
import org.example.builder.WordDtoBuilder;
import org.example.dto.WordDto;
import org.example.entity.Word;

import java.util.ArrayList;
import java.util.List;

public class WordMapper {

    private WordMapper() {

    }

    public static List<WordDto> toDtos(List<Word> words) {
        List<WordDto> dtos = new ArrayList<>(words.size());
        words.forEach(word -> dtos.add(new WordDtoBuilder()
                .original(word.getOriginal())
                .translation(word.getTranslation())
                .dictionaryType(word.getDictionaryType())
                .build()));
        return dtos;
    }

    public static Word toEntity(WordDto wordDto) {
        return new WordBuilder()
                .original(wordDto.getOriginal())
                .translation(wordDto.getTranslation())
                .dictionaryType(wordDto.getDictionaryType())
                .build();
    }
}
