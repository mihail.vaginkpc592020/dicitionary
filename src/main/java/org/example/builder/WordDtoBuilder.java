package org.example.builder;

import org.example.dto.WordDto;
import org.example.entity.DictionaryType;

public class WordDtoBuilder {

    private WordDto dto = new WordDto();

    public WordDtoBuilder original(String original) {
        dto.setOriginal(original);
        return this;
    }

    public WordDtoBuilder translation(String translation) {
        dto.setTranslation(translation);
        return this;
    }

    public WordDtoBuilder dictionaryType(DictionaryType type){
        dto.setDictionaryType(type);
        return this;
    }

    public WordDto build() {
        return dto;
    }
}
