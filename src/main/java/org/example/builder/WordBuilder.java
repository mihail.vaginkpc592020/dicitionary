package org.example.builder;

import org.example.entity.DictionaryType;
import org.example.entity.Word;

public class WordBuilder {

    private Word word = new Word();

    public WordBuilder original(String original) {
        word.setOriginal(original);
        return this;
    }

    public WordBuilder translation(String translation) {
        word.setTranslation(translation);
        return this;
    }

    public WordBuilder dictionaryType(DictionaryType type){
        word.setDictionaryType(type);
        return this;
    }

    public Word build() {
        return word;
    }
}
