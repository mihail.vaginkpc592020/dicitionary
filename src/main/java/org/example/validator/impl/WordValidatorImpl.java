package org.example.validator.impl;

import org.example.dto.WordDto;
import org.example.exception.NotExistDictionaryValidation;
import org.example.validator.api.WordsValidator;
import org.example.validator.strategy.api.Validation;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
public class WordValidatorImpl implements WordsValidator {

    private final Map<String, Validation> validateStrategies;

    public WordValidatorImpl(Map<String, Validation> validateStrategies) {
        this.validateStrategies = validateStrategies;
    }

    @Override
    public void validate(WordDto dto) {
        Validation validation = validateStrategies.get(dto.getDictionaryType().name().toLowerCase());
        if (validation == null) {
            throw new NotExistDictionaryValidation(dto.getDictionaryType().name());
        }
        validation.validate(dto);
    }
}
