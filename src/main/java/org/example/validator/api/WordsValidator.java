package org.example.validator.api;

import org.example.dto.WordDto;

public interface WordsValidator {

    void validate(WordDto word);
}
