package org.example.validator.strategy.impl;

import org.example.dto.WordDto;
import org.example.exception.InvalidWordException;
import org.example.validator.strategy.api.Validation;
import org.springframework.stereotype.Component;

@Component("numbers")
public class NumbersValidation implements Validation {

    private static final String TRANSLATION_ERROR = "Перевод слова должен состоять из 4-х латинских букв";
    private static final String VALUE_ERROR = "Значение слова должно состоять из 5-ти цифр";

    @Override
    public void validate(WordDto word) {
        if (word.getTranslation().length() != 4 || !word.getTranslation().matches("\\w+")) {
            throw new InvalidWordException(TRANSLATION_ERROR);
        }

        if (word.getOriginal().length() != 5 || !word.getOriginal().matches("\\d+")) {
            throw new InvalidWordException(VALUE_ERROR);
        }
    }
}
