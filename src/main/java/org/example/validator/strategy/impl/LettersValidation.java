package org.example.validator.strategy.impl;

import org.example.dto.WordDto;
import org.example.exception.InvalidWordException;
import org.example.validator.strategy.api.Validation;
import org.springframework.stereotype.Component;

@Component("letters")
public class LettersValidation implements Validation {

    private static final String TRANSLATION_ERROR = "Перевод слова должен состоять из 5-ти цифр";
    private static final String VALUE_ERROR = "Значение слова должно состоять из 4-х латинских букв";


    @Override
    public void validate(WordDto word) {
        if (word.getTranslation().length() != 5 || !word.getTranslation().matches("\\d+")) {
            throw new InvalidWordException(TRANSLATION_ERROR);
        }

        if (word.getOriginal().length() != 4 || !word.getOriginal().matches("\\w+")) {
            throw new InvalidWordException(VALUE_ERROR);
        }
    }
}
