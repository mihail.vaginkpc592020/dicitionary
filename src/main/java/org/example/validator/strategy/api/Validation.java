package org.example.validator.strategy.api;

import org.example.dto.WordDto;

public interface Validation {
    void validate(WordDto word);
}
